const { getDatabaseClient } = require("../Utils/dbConnection");
const { handleError } = require("../Utils/utilMethods");
const { validateString } = require("../Utils/validations");
const constants = require("../Config/constants");


exports.bookList = async (req, res) => {
    let client = getDatabaseClient();
    try {
        let filter={"match_all":{}};
    if (req.query.category){
        filter={"match": {
            "categories": req.query.category
          }
          }
    }

        let result = await client.search({
            "index": "majestiic_book_house",
            body:{
                "query": {
                  "bool": {
                    "must": [
                     filter
                    ]
                  }
                },
                "aggs": {
                  "categories": {
                    "terms": {
                      "field": "categories.keyword",
                      "size": 10
                    }
                  }
                }
              }

        });
        let cat=result.aggregations.categories.buckets;
        console.log(cat);
        var categories_count = cat.map(obj =>{ 
            var rObj = {};
            rObj[obj.key] = obj.doc_count;
            return rObj;
         });
        res.send({books:result.hits.hits,categories:categories_count});
    } catch (err) {
        handleError(res, err.toString());
    } finally {
        
    }
}


